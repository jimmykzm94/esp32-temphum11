#include <Arduino.h>
#include "ble.h"

void setup(){
    Serial.begin(115200);
    ble_init();

    if(xTaskCreate(ble_task, "BLE_TASK", 2048, NULL, tskIDLE_PRIORITY, NULL) != pdTRUE){
        Serial.println("Task creation failure.");
        while(1);
    }
}

void loop(){
    
}