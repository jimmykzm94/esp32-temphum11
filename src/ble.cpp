/**
 * @file ble.cpp
 * @brief BLE implementation
 */
#include <Arduino.h>
#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>
#include "ble.h"
#include "temphum11.h"

#define SERVICE_UUID "181A"   //!< Environment service UUID
#define TEMP_CHAR_UUID "2A6E" //!< Temperature characteristic UUID
#define HUM_CHAR_UUID "2A6F"  //!< Humidity characteristic UUID

BLEServer* pServer = NULL;  //!< Server pointer
BLECharacteristic* pTempCharacteristic = NULL; //!< Pointer of temperature characteristic
BLECharacteristic* pHumCharacteristic = NULL; //!< Pointer of humidity characteristic
bool deviceConnected = false; //!< Device connected state

/**
 * @brief Server callback
 * 
 */
class MyServerCallbacks: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
      deviceConnected = true;
    };

    void onDisconnect(BLEServer* pServer) {
      deviceConnected = false;
      pServer->startAdvertising();
    }
};

void ble_init(){
    // Init sensor
    temphum11_open(21,22,0);
    
    // Init BLE
    BLEDevice::init("NodeMCU BLE");
    pServer = BLEDevice::createServer();
    pServer->setCallbacks(new MyServerCallbacks());
    BLEService *pService = pServer->createService(SERVICE_UUID);

    // Temperature characteristic and descriptor
    pTempCharacteristic = pService->createCharacteristic(
                            TEMP_CHAR_UUID,
                            BLECharacteristic::PROPERTY_NOTIFY
                            );

    pTempCharacteristic->addDescriptor(new BLE2902());

    // Humidity characteristic and descriptor
    pHumCharacteristic = pService->createCharacteristic(
                            HUM_CHAR_UUID,
                            BLECharacteristic::PROPERTY_NOTIFY
                            );

    pHumCharacteristic->addDescriptor(new BLE2902());
    
    // Start service
    pService->start();

    // Start advertising
    BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
    pAdvertising->addServiceUUID(SERVICE_UUID);
    pAdvertising->setScanResponse(false);
    pAdvertising->setMinPreferred(0x0);  // set value to 0x00 to not advertise this parameter
    BLEDevice::startAdvertising();
}

void ble_task(void *params){
    // init
    float temperature = 0;
    float humidity = 0;
    uint16_t buff;
    while(1){
        // temperature
        temperature = temphum11_getTemperature(TEMPHUM11_TEMP_IN_CELSIUS);
        buff = (uint16_t)(temperature * 100);
        pTempCharacteristic->setValue(buff);
        pTempCharacteristic->notify();

        // humidity
        humidity = temphum11_getHumidity();
        buff = (uint16_t)(humidity * 100);
        pHumCharacteristic->setValue(buff);
        pHumCharacteristic->notify();

        delay(500);
    }
}