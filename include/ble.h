/**
 * @file ble.h
 * @brief BLE header
 */
#pragma once

/**
 * @brief BLE initialization
 * 
 */
void ble_init();

/**
 * @brief BLE task
 * 
 * @param params Parameters, required for task creation
 */
void ble_task(void *params);