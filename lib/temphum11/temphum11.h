/**
 * @file temphum11.h
 * @brief Header for TempHum11 sensor
 * @note Adapted from https://github.com/MikroElektronika/mikrosdk_click_v2/blob/master/clicks/temphum11/lib_temphum11/include/temphum11.h
 */

#pragma once
#include <stdint.h>

/* Define */

#define TEMPHUM11_DEVICE_SLAVE_ADDR             0x40 //!< Sensor I2C address

#define TEMPHUM11_REG_TEMPERATURE               0x00 //!< Temperature register
#define TEMPHUM11_REG_HUMIDITY                  0x01 //!< Humidity register
#define TEMPHUM11_REG_CONFIGURATION             0x02 //!< Configuration register
#define TEMPHUM11_REG_SERIAL_ID_0               0xFB //!< Serial ID 0 register
#define TEMPHUM11_REG_SERIAL_ID_1               0xFC //!< Serial ID 1 register
#define TEMPHUM11_REG_SERIAL_ID_2               0xFD //!< Serial ID 2 register
#define TEMPHUM11_REG_MANUFACTURER_ID           0xFE //!< Manufactured ID register
#define TEMPHUM11_REG_DEVICE_ID                 0xFF //!< Device ID register

#define TEMPHUM11_NORMAL_OPERATION              0x0000 //!< Normal operation bits
#define TEMPHUM11_SOFTWARE_RESET                0x8000 //!< Software reset bits
#define TEMPHUM11_HEATER_DISABLED               0x0000 //!< Heater disabled bits
#define TEMPHUM11_HEATER_ENABLED                0x2000 //!< Heater enabled bits
#define TEMPHUM11_TEMP_HUM_ACQUIRED             0x0000 //!< Acquired temperature and humidity separately
#define TEMPHUM11_TEMP_FIRST                    0x1000 //!< Acquired temperature following by humidity
#define TEMPHUM11_BAT_VOLT_BIGGER_THAN_2p8      0x0000 //!< TODO
#define TEMPHUM11_BAT_VOLT_SMALLER_THAN_2p8     0x0800 //!< TODO
#define TEMPHUM11_TEMP_RESOLUTION_14bit         0x0000 //!< Temperature 14bit resolution bits
#define TEMPHUM11_TEMP_RESOLUTION_11bit         0x0400 //!< Temperature 10bit resolution bits
#define TEMPHUM11_HUM_RESOLUTION_14bit          0x0000 //!< Humidity 14bit resolution bits
#define TEMPHUM11_HUM_RESOLUTION_11bit          0x0100 //!< Humidity 11bit resolution bits
#define TEMPHUM11_HUM_RESOLUTION_8bit           0x0200 //!< Humidity 8bit resolution bits

#define TEMPHUM11_DEVICE_ID                     0x1050 //!< Device ID
#define TEMPHUM11_MANUFACTURER_ID               0x5449 //!< Manufactured ID

#define TEMPHUM11_TEMP_IN_CELSIUS               0x00 //!< Temperature scale in Celcius
#define TEMPHUM11_TEMP_IN_KELVIN                0x01 //!< Temperature scale in Kelvin
#define TEMPHUM11_TEMP_IN_FAHRENHEIT            0x02 //!< Temperature scale in Fahrenheit

/// @brief Open port
/// @param sda SDA pin
/// @param scl SCL pin
/// @param i2c_port Port number: 0(Wire) or 1(Wire1)
void temphum11_open(uint8_t sda, uint8_t scl, uint8_t i2c_port);

/// @brief Write configurations
/// @param config Configuration bits
void temphum11_writeConfig(uint16_t config);

/// @brief Set default configuration
void temphum11_setDefaultConfig();

/// @brief Read data from sensor
/// @param reg Register
/// @return Data
uint16_t temphum11_readData(uint8_t reg);

/// @brief Get temperature reading
/// @param temp_in Temperature scale: Kelvin, Fahrenheit, Celcius
/// @return Temperature
float temphum11_getTemperature(uint8_t temp_in);

/// @brief Get humidity reading
/// @return Humidity
float temphum11_getHumidity();

/// @brief Get sensor data
/// @param[out] temperature Temperature
/// @param[out] humidity Humidity
void temphum11_getData(float *temperature, float *humidity);

/// @brief Get device ID
/// @return Device ID
uint16_t temphum11_getDeviceId();

/// @brief Get manufactured ID
/// @return Manufactured ID
uint16_t temphum11_getManufactureId();
