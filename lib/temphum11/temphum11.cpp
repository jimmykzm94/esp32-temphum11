/**
 * @file temphum11.cpp
 * @brief CPP for TempHum11 sensor
 */
#include "temphum11.h"
#include "Wire.h"

static TwoWire* wire = NULL;

void temphum11_open(uint8_t sda, uint8_t scl, uint8_t i2c_port){
    wire = i2c_port == 0 ? &Wire : & Wire1;
    wire->begin(sda, scl, 100000);
}

void temphum11_writeConfig(uint16_t config){
    uint8_t write_reg[3];

    write_reg[0] = TEMPHUM11_REG_CONFIGURATION;
    write_reg[1] = (uint8_t)(config >> 8);
    write_reg[2] = (uint8_t)config;

    wire->beginTransmission(TEMPHUM11_DEVICE_SLAVE_ADDR);
    for(uint8_t i = 0; i < 3; i++){
        wire->write(write_reg[i]);
    }
    wire->endTransmission();
}

void temphum11_setDefaultConfig(){
    temphum11_writeConfig(  TEMPHUM11_NORMAL_OPERATION | TEMPHUM11_HEATER_DISABLED |
                            TEMPHUM11_TEMP_HUM_ACQUIRED | TEMPHUM11_TEMP_RESOLUTION_14bit |
                            TEMPHUM11_HUM_RESOLUTION_14bit);
}

uint16_t temphum11_readData(uint8_t reg){
    uint8_t read_reg[2] = {0};
    uint16_t read_data = 0;

    wire->beginTransmission(TEMPHUM11_DEVICE_SLAVE_ADDR);
    wire->write(reg);
    wire->endTransmission();

    delay(10);

    wire->requestFrom(TEMPHUM11_DEVICE_SLAVE_ADDR, 2U);
    wire->readBytes(read_reg, 2);

    read_data = read_reg[0] << 8;
    read_data = read_data | read_reg[1];

    return read_data;
}

float temphum11_getTemperature(uint8_t temp_in){
    uint16_t temp_out = UINT16_MAX;
    float temperature;

    temp_out = temphum11_readData(TEMPHUM11_REG_TEMPERATURE);
    temperature = (float)(temp_out / 65536.0) * 165.0 - 40.0;

    if(temp_in == TEMPHUM11_TEMP_IN_KELVIN){
        temperature = temperature + 273.15;
    }
    else if(temp_in == TEMPHUM11_TEMP_IN_FAHRENHEIT){
        temperature = (temperature * 9.0 / 5.0) + 32;
    }

    return temperature;
}

float temphum11_getHumidity(){
    uint16_t hum_out = UINT16_MAX;
    float humidity;

    hum_out = temphum11_readData(TEMPHUM11_REG_HUMIDITY);
    humidity = (float)(hum_out / 65536.0) * 100.0;

    return humidity;
}

void temphum11_getData(float *temperature, float *humidity){
    uint8_t read_reg[4] = {0};
    uint32_t read_data = 0;

    // set mode
    temphum11_writeConfig(TEMPHUM11_TEMP_FIRST);
    delay(10);

    wire->beginTransmission(TEMPHUM11_DEVICE_SLAVE_ADDR);
    wire->write(TEMPHUM11_REG_TEMPERATURE);
    wire->endTransmission();

    delay(15);

    wire->requestFrom(TEMPHUM11_DEVICE_SLAVE_ADDR, 4U);
    wire->readBytes(read_reg, 4);

    *temperature = (float)((uint16_t)(read_reg[0] << 8 | read_reg[1]) / 65536.0) * 165.0 - 40;
    *humidity = (float)((uint16_t)(read_reg[2] << 8 | read_reg[3]) / 65536.0) * 100.0;
}

uint16_t temphum11_getDeviceId(){
    uint16_t res = temphum11_readData(TEMPHUM11_REG_DEVICE_ID);
    return res;
}

uint16_t temphum11_getManufactureId(){
    uint16_t res = temphum11_readData(TEMPHUM11_REG_MANUFACTURER_ID);
    return res;
}
