#!/bin/sh
# Doxygen always return exit code 0. By grepping warning messages, it will fail the test if found.

# Run doxygen
doxygen Doxyfile >& docx.txt

# Result
message=$(cat docx.txt | grep warning)
if [ "$message" == "" ]; then
    echo "{\"result\":1}"
    exit 0
else
    echo "{\"result\":0}"
    exit 1
fi
